import java.util.*;

class OOPGeometry {

	public static void main(String[]arrrrgs){
		Scanner input=new Scanner(System.in);
		
		MainMenu mainMenu=new MainMenu();
		
		try{
			mainMenu.display();
		}catch(NoSuchElementException e){
			System.out.println();
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program Terminated");
		}
	}	

}



class MainMenu{	
	static Scanner input=new Scanner(System.in);	
	void display(){

		threeDimensional threeDimension=new threeDimensional();
		twoDimensional twoDimension=new twoDimensional();
		
		do{
			String choice="a";
	    	int select=-1;
	    System.out.println("\n");
		System.out.println("        Bangun Ruang 2 Dan 3 Dimensi");
		System.out.println("========================================"); 		
		System.out.println("1. Bangun Ruang 2 Dimensi");
		System.out.println("2. Bangun Ruang 3 Dimensi ");
		System.out.println("0. Exit");
		System.out.println("========================================");
			System.out.print("Masukkan Angka Sesuai Menu : ");
			try{
				select=input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("Input yang di masukkan bukan angka");
				System.out.print(" ");
			}	    		

		 input.nextLine();			
		    	
		switch(select){
		case 0:
			break;
		case 1:
			twoDimension.display();
			break;
		case 2:   			
	        threeDimension.display();
	        break;
		default:
			System.out.println("Masukkan Angka Sesuai Nomor Menu yang Di butuhkan");
			do{ 
			    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("----------------------------------------");
			    
				    if (choice.contentEquals("y")){
				    	break;
				    }
				    else if(choice.contentEquals("n")){
				    	break;
				    }
				    else {
				    	System.out.println("Masukan tidak sesuai, silahkan tekan hruf 'y' atau 'n'\n\n" );
				    }
				    
	    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
			break;
	   }
				if (select == 0)break;
			    if (choice.contentEquals("n"))break;
			    if (choice.contentEquals("y"))continue;
		
		
			
		}while(true);
	}
}
class twoDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);	 
	 void display(){
		  
		  rectangle rectangle = new rectangle();
		  isoTriangle isoTriangle = new isoTriangle();
		  equiTriangle equiTriangle = new equiTriangle();
		  square square = new square();
		  parallelogram parallelogram = new parallelogram();
		  trapezoid trapezoid = new trapezoid();
		  circle circle = new circle();
		  ellipse ellipse = new ellipse();
		  polygon polygon = new polygon();
 
			do{
				String choice="a";
		    	int select=-1;
		    	System.out.println("\n");
				System.out.println("        Bangun Ruang 2 Dimensi");
				System.out.println("========================================"); 		
				System.out.println("1. Rectangle (Persegi Panjang)");
				System.out.println("2. Isosceles Triangle (Segitiga Sama Kaki)");
				System.out.println("3. Equilateral Triangle (Segitiga Sama Sisi)");
				System.out.println("4. Square (Persegi)");
				System.out.println("5. Parallelogram (Jajar Genjang)");
				System.out.println("6. Trapezoid (Trapesium)");
				System.out.println("7. Circle (Lingkaran)");
				System.out.println("8. Ellipse (Elips)");
				System.out.println("9. Regular Polygon (Poligon)");
				System.out.println("0. Back to Main Menu");
				System.out.println("========================================");
					System.out.print("Masukkan Angka Sesuai Nomor Menu yang Di butuhkan : ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println("----------------------------------------");
		    		}catch(InputMismatchException e){
		    			System.out.println("Input yang di masukkan bukan angka");
		    			System.out.print(" ");
		    		}	    		
			
				 input.nextLine();			
				    	
				switch(select){
				case 0:
					break;
				case 1:
					rectangle.calculate();
					break;
				case 2:   			
		            isoTriangle.calculate();
		            break;
				case 3:
					equiTriangle.calculate();
					break;
				case 4:
					square.calculate();
					break;
				case 5:
					parallelogram.calculate();
					break;
				case 6:
					trapezoid.calculate();
					break;
				case 7:
					circle.calculate();
					break;
				case 8:
					ellipse.calculate();
					break;
				case 9:
					polygon.calculate();
					break;
				default:
					System.out.println("Masukkan Tidak Sesuai");
					do{ 
					    System.out.print("Ulang kembali ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
					    
						    if (choice.contentEquals("y")){
						    	break;
						    }
						    else if(choice.contentEquals("n")){
						    	break;
						    }
						    else {
						    	System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n" );
						    }
						    
			    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
					break;
			   }
				
				
						if (select == 0)break;
					    if (choice.contentEquals("n"))break;
					    if (choice.contentEquals("y"))continue;
				
				
					
			}while(true);
		}
}
class threeDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);
	 void display(){
		  
	  	 cube cube = new cube();
	   	 rectSolid rectSolid = new rectSolid();
		 sphere sphere = new sphere();
		 cylinder cylinder = new cylinder();
		 torus torus = new torus();
		 circCone circCone = new circCone();
		 frustum frustum = new frustum();
		 squarePyramid squarePyramid = new squarePyramid();
		 tetrahedron tetrahedron = new tetrahedron();
		 
			do{
				
	 		String choice="a";
	     	int select=-1;
	     	System.out.println("\n");
	 		System.out.println("        Bangun Ruang 3 Dimensi ");
	 		System.out.println("========================================"); 		
	 		System.out.println("1. Cube (Kubus)");
	 		System.out.println("2. Rectangular Solid (Balok)");
	 		System.out.println("3. Sphere (Bola)");
	 		System.out.println("4. Right Circular Cylinder (Tabung)");
	 		System.out.println("5. Torus (Ban)");
	 		System.out.println("6. Right Circular Cone (Kerucut)");
	 		System.out.println("7. Frustum of a Cone (Potongan Kerucut)");
	 		System.out.println("8. Square Pyramid (Limas Segiempat)");
	 		System.out.println("9. Regular Tetrahedron(Limas Segitiga)");
	 		System.out.println("0. Back to Main Menu");
	 		System.out.println("========================================");
	 			System.out.print("Masukkan Angka Sesuai Nomor Menu yang Di butuhkan :  ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println("----------------------------------------");
		    		}catch(InputMismatchException e){
		    			System.out.println("Input yang di masukkan bukan angka");
		    			System.out.print(" ");
		    		}	    		
	 	
	 		 input.nextLine();			
	 		
	 		switch(select){
	 		case 0:
	 			break;
	 		case 1:
	 			cube.calculate();
	 			break;
	 		case 2:   			
	             rectSolid.calculate();
	             break;
	 		case 3:
	 			sphere.calculate();
	 			break;
	 		case 4:
	 			cylinder.calculate();
	 			break;
	 		case 5:
	 			torus.calculate();
	 			break;
	 		case 6:
	 			circCone.calculate();
	 			break;
	 		case 7:
	 			frustum.calculate();
	 			break;
	 		case 8:
	 			squarePyramid.calculate();
	 			break;
	 		case 9:
	 			tetrahedron.calculate();
	 			break;
	 			
	 		default:
	 			System.out.println("Masukkan Tidak Sesuai");
	 			do{ 
	 			    System.out.print("Ulang Kembali ? (y/n)");
	 			    System.out.println();			   
	 			    choice=input.nextLine();
	 			    System.out.println("----------------------------------------");
	 			    
	 				    if (choice.contentEquals("y")){
	 				    	break;
	 				    }
	 				    else if(choice.contentEquals("n")){
	 				    	break;
	 				    }
	 				    else {
	 				    	System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
	 				    }
	 				    
	 	    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
	 			break;
	 			
	 	   }
	 	   
	 				if (select == 0)break;
	 			    if (choice.contentEquals("n"))break;
	 			    if (choice.contentEquals("y"))continue;
	 		
	 		
	 			
	 	}while(true); 
	 }
}
 



class cubeFormula{
		double sisi;
		double volume(){
			return sisi*sisi*sisi;
		}
		double surfArea(){
			return sisi*sisi*6.0;
		}
}
		class cube extends cubeFormula{
				static Scanner input=new Scanner(System.in);
				 void calculate(){
				double sisi;
			    String choice;
			    do{
				    try{
				    	System.out.println("                 Cube");
				    	System.out.println("Please input the length of the side");
				    	System.out.print(">> ");
				    	sisi=input.nextDouble();
				    	this.sisi=sisi;
				    	if (this.sisi <= 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the cube is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the cube is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				   
				    catch(IllegalArgumentException e){
				    	if(this.sisi<0)System.out.println("Inputan tidak bisa berupa angka negatif");
				    	else if(this.sisi==0)System.out.println("Inputan tidak bisa berupa angka 0");
				    	
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if ( choice.contentEquals("y") )break;
						    else if( choice.contentEquals("n") )break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || ( choice.contentEquals("n") ) == false);
				    
				    if ( choice.contentEquals("y") )continue;
				    else if( choice.contentEquals("n") )break;
					    
				    
			    }while(true);
				}
		}
	


class rectSolidFormula{
	 double length,lebar,height;
	 double volume(){
		 return length*lebar*height;
	 }
	 double surfArea(){
		 return (2 * length * lebar) + (2 * length * height) + (2 * lebar* height);
	 }
}	
		class rectSolid extends rectSolidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double length,lebar,height;
		        String choice;
		        do{
				    try{
				    	System.out.println("              Rectangular Solid");
				    	System.out.println("Please input the length of the Rectangular Solid");
				    	System.out.print(">> ");
				    	length=input.nextDouble();  	
				    	this.length=length;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the lebar of the Rectangular Solid");
				    	System.out.print(">> ");
				    	lebar=input.nextDouble();  
				    	this.lebar=lebar;
				    	if (lebar < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (lebar == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Rectangular Solid");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Rectangular Solid is %.1f\n",volume());
			
				    	System.out.printf("The surface area of the Rectangular Solid is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
		        }while(true);
			}
		}



class sphereFormula{
	double pi=(double)22/7,radius;
	double volume(){
		return ( (double) 4/3 ) * pi * radius * radius * radius;
	}
	double surfArea(){
		return 4.0 * pi * radius * radius;
	}
}
		class sphere extends sphereFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("                 Sphere");
				    	System.out.println("Please input the radius of the Sphere");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    		
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Sphere is %.2f\n",volume());
				    	
				    	System.out.printf("The surface area of the Sphere is %.2f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class cylinderFormula{
	double pi=(double)22/7,radius,height;
	double volume(){
		return pi * radius * radius * height;
	}
	double surfArea(){
		return (2.0 * pi * radius * height) + (2.0 * pi * radius * radius);
	}
}
		class cylinder extends cylinderFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double radius,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("              Right Circular Cylinder");
				    	System.out.println("Please input the radius of the base of the Right Circular Cylinder");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Right Circular Cylinder");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Right Circular Cylinder is %.1f\n",volume());
				    	
				    	
				    	System.out.printf("The surface area of the Right Circular Cylinder is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class torusFormula{
	double pi=(double)22/7,tubeRadius,torusRadius;
	double volume(){
		return (double)2.0 * pi * pi * tubeRadius * tubeRadius * torusRadius;
	}
	double surfArea(){
		return (double)4 * pi * pi * tubeRadius * torusRadius;
	}
}
		class torus extends torusFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double tubeRadius,torusRadius;
			    String choice;
			    do{
				    try{
				    	System.out.println("                 Torus");
				    	System.out.println("Please input the tube radius of the Torus");
				    	System.out.print(">> ");
				    	tubeRadius=input.nextDouble();
				    	this.tubeRadius=tubeRadius;
				    	if (tubeRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (tubeRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the radius of the Torus");
				    	System.out.print(">> ");
				    	torusRadius=input.nextDouble();
				    	this.torusRadius=torusRadius;
				    	if (torusRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (torusRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Torus is %.1f\n",volume());
				    	
				    	
				    	System.out.printf("The surface area of the Torus is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class circConeFormula{
	double pi = (double)22/7,radius,height;
	double volume(){
		return ((double)1/3) * pi * radius * radius * height;
	}
	double surfArea(){
		return pi * radius * (Math.sqrt((radius * radius) + (height * height))) + (pi * radius * radius);
	}
	
}
		class circCone extends circConeFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double radius,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("            Circular Cone");
				    	System.out.println("Please input the radius of the Cone");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();  	
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Cone");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Cone is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Cone is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class frustumFormula{
	double pi = (double)22/7,topRadius,slantHeight,height,baseRadius;
	double volume(){
		return ((double)pi/3) * ( (topRadius * topRadius) + (topRadius * baseRadius) + (baseRadius * baseRadius)) * height;
	}
	double surfArea(){
		return (pi * slantHeight * (topRadius + baseRadius) ) + (pi * topRadius * topRadius) + (pi * baseRadius * baseRadius);
	}
}
		class frustum extends frustumFormula{
			static Scanner input=new Scanner(System.in);
			void calculate(){
		
				double baseRadius,height,topRadius,slantHeight;
			    String choice;
			    do{
				    try{
				    	System.out.println("             Frustum of a Cone");
				    	System.out.println("Please input the base radius of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	baseRadius=input.nextDouble(); 
				    	this.baseRadius=baseRadius;
				    	if (baseRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (baseRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the top radius of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	topRadius=input.nextDouble();
				    	this.topRadius=topRadius;
				    	if (topRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (topRadius == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println("Please input the height of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println("Please input the Slant Height of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	slantHeight=input.nextDouble();
				    	this.slantHeight=slantHeight;
				    	if (slantHeight < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (slantHeight == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Frustum of the Cone is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Frustum of the Cone is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class squarePyramidFormula{
	double sisi,height;
	double volume(){
		return (double)1/3 * sisi * sisi * height;
	}
	double surfArea(){
		return sisi * (sisi + Math.sqrt( (sisi * sisi) + (4 * height * height) ) );
	}
}
		class squarePyramid extends squarePyramidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double sisi,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("             Square Pyramid");
				    	System.out.println("Please input the base side length of the Square Pyramid");
				    	System.out.print(">> ");
				    	sisi=input.nextDouble();
				    	this.sisi=sisi;
				    	if (sisi < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (sisi == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println("Please input the height of the Square Pyramid");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Square Pyramid is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Square Pyramid is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class tetrahedronFormula{
	double sisi;
	double volume(){
		return ((double)1/12) * Math.sqrt(2 * sisi * sisi *sisi);
	}
	double surfArea(){
		return Math.sqrt(3.0 * sisi * sisi);
	}
}
		class tetrahedron extends tetrahedronFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double sisi;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Regular Tetrahedron");
				    	System.out.println("Please input the number of side length of the Tetrahedron");
				    	System.out.print(">> ");
				    	sisi=input.nextDouble();  	
				    	if (sisi < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (sisi == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Tetrahedron is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Tetrahedron is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class rectangleFormula{
	double lebar,length;
	double area(){
		return lebar*length;
	}
	double perimeter(){
		return (2.0 * lebar) + (2.0 * length);
	}

}
		class rectangle extends rectangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double lebar,length;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Rectangle");
				    	System.out.println("Masukkan besar lebar yang di ketahui");
				    	System.out.print(">> ");
				    	lebar=input.nextDouble();
				    	this.lebar=lebar;
				    	if (lebar < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (lebar == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukkan nilai panjang yang di ketahui");
				    	System.out.print(">> ");
				    	length=input.nextDouble();
				    	this.length=length;
				    	;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the rectangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the rectangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)(y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class isoTriangleFormula{
	double base,height,sisilainnya;
	double area(){
		return 0.5 * base * height;
	}
	double perimeter(){
		return base + (2.0 * sisilainnya);
	}
}
		class isoTriangle extends isoTriangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double base,height,sisilainnya;
			    String choice;
			    do{
				    try{
				    	System.out.println("             Isosceles Triangle");
				    	System.out.println("Masukkan nilai base yang di ketahui");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
				    	if (base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (base == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukkan nilai tinggi yang di ketahui");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the size of side B (B = C)");
				    	System.out.print(">> ");
				    	sisilainnya=input.nextDouble();
				    	
				    	if (sisilainnya < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (sisilainnya == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Isosceles Triangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Isosceles Triangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n))");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class equiTriangleFormula{
	double sisi,height;
	double area(){
		return 0.5 * sisi * height;
	}
	double perimeter(){
		return sisi + sisi + sisi;
	}
}
		class equiTriangle extends equiTriangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
			    double sisi,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("          Equilateral Triangle");
				    	System.out.println("Masukkan nilai sisi yang di ketahui");
				    	System.out.print(">> ");
				    	sisi=input.nextDouble();
				    	this.sisi=sisi;
				    	if (sisi < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (sisi == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Masukkan nilai tinggi yang diketahui");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Equilateral Triangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Equilateral Triangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class squareFormula{
	double sisi;
	double area(){
		return sisi * sisi ;
	}
	double perimeter(){
		return sisi + sisi + sisi + sisi;
	}
}
		class square extends squareFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double sisi;
			    String choice;
			    do{
				    try{
				    	System.out.println("                 Square");
				    	System.out.println("Masukkan nilai sisi yang di ketahui");
				    	System.out.print(">> ");
				    	sisi=input.nextDouble();
				    	this.sisi=sisi;
				    	
				    	
				    	if (sisi < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (sisi == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Square is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the square is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class parallelogramFormula{
	double base,height,sisilainnya;
	double area(){
		return base * height;
	}
	double perimeter(){
		return (2.0 * base ) + (2.0 * sisilainnya);
	}
}
		class parallelogram extends parallelogramFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				double base,height,sisilainnya;
			    String choice;
			    do{
				    try{
				    	System.out.println("              Parallelogram");
				    	System.out.println("Masukka nilai base yang di ketahui");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
					    	if (base < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (base == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Masukkan nilai tinggi yang diketahui");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
					    	if (height < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (height == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Please input the size of side B");
				    	System.out.print(">> ");
				    	sisilainnya=input.nextDouble();
				    	this.sisilainnya=sisilainnya;
					    	if (sisilainnya < 0) {
					    	    throw new IllegalArgumentException();
					    	}	    	
					    	if (sisilainnya == 0) {
					    	    throw new ArithmeticException();
					    	}
					    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Parallelogram is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Parallelogram is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n))");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class trapezoidFormula{
	double base1,base2,height,sisilainnya;
	double area(){
		return 0.5 * (base1 + base2) * height;
	}
	double perimeter(){
		return base1 + base2 + (2.0 * sisilainnya);
	}
}
		class trapezoid extends trapezoidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double base1,base2,height,sisilainnya;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Trapezoid");
				    	System.out.println("Masukkan nilai base (bottom)");
				    	System.out.print(">> ");
				    	base1=input.nextDouble();
				    	this.base1=base1;
					    	if (base1 < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (base1 == 0) {
					    	    throw new ArithmeticException();
					    	}
				    	
				    	System.out.println("Masukkan nilai ukuran base (top)");
				    	System.out.print(">> ");
				    	base2=input.nextDouble();
				    	this.base2=base2;
					    	if (base2 < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (base2 == 0) {
					    	    throw new ArithmeticException();
					    	}
					    	
				    	System.out.println("Masukkan nilai ukuran tinggi");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
					    	if (height < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (height == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Please input the size of side C (C = D)");
				    	System.out.print(">> ");
				    	sisilainnya=input.nextDouble();
				    	this.sisilainnya=sisilainnya;
					    	if (sisilainnya < 0) {
					    	    throw new IllegalArgumentException();
					    	}		    	
					    	if (sisilainnya == 0) {
					    	    throw new ArithmeticException();
					    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Trapezoid is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Trapezoid is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			    
			}
		}



class circleFormula{
	double radius,pi=(double)22/7;
	double area(){
		return pi * radius * radius;
	}
	double perimeter(){
		return 2.0 * pi * radius;
	}
}
		class circle extends circleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Circle");
				    	System.out.println("Masukkan nilai radius");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
		
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    
				    	System.out.printf("The area of the Circle is %.3f\n",area());
				    	
				    	System.out.printf("The circumference of the Circle is %.3f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class ellipseFormula{
	double semiMajor,semiMinor,pi=(double)22/7;
	double area(){
		return pi * semiMajor *  semiMinor ;
	}
	double circumference(){
		return pi * ((3.0 * (semiMajor + semiMinor)) - Math.sqrt(((semiMajor + (3.0 * semiMinor)) * ((3 * semiMajor) + semiMinor))));
	}
}
		class ellipse extends ellipseFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double semiMajor,semiMinor;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Ellipse");
				    	System.out.println("Masukkan nilai Semimajor Axis");
				    	System.out.print(">> ");
				    	semiMajor=input.nextDouble();
				    	this.semiMajor = semiMajor;
				    	if (semiMajor < 0) {
				    	    throw new IllegalArgumentException();
				    	}if (semiMajor == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println("Masukkan nilai Semiminor Axis");
				    	System.out.print(">> ");
				    	semiMinor=input.nextDouble();
				    	this.semiMinor= semiMinor;
				    	if (semiMinor < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (semiMinor == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Ellipse is %.3f\n",area());
				    	
				    	System.out.printf("The circumference of the Ellipse is %.3f\n",circumference());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Inputan tidak bisa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Inputan tidak bisa berupa angka negatif");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class polygonFormula{
	double nsisi,panjangsisi,pi=(double)22/7;
	double area(){
		return 0.25 * nsisi * panjangsisi * panjangsisi * ((double)1 / Math.tan(pi/nsisi));
	}
	double perimeter(){
		return nsisi * panjangsisi;
	}
}
		class polygon extends polygonFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double nsisi=-1,panjangsisi=-1;
			    String choice;
			    do{
				    try{
				    	System.out.println("               Regular Polygon");
				    	System.out.println("Please input the number of sides of the polygon ( >4 )");
				    	System.out.print(">> ");
				    	nsisi=input.nextInt();
				    	
				    	if (nsisi < 5) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	System.out.println("Please input the length of each side");
				    	System.out.print(">> ");
				    	panjangsisi=input.nextDouble();	    	
				    	if (panjangsisi < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (panjangsisi == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Polygon is %.3f\n",area());
				    	
				    	System.out.printf("The perimeter of the Polygon is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Error: The number of sides must be an integer");
				    	System.out.println("Error: inputan yang di masukan bukan angka");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Error: Inputan tidak bisa berupa angka 0");
				    }
				    catch(IllegalArgumentException e){
				    	if(nsisi<5 && nsisi>=0)System.out.println("Error: The number of sides is less than 5");
				    	if(nsisi<0 || panjangsisi<0)System.out.println("Error: Inputan tidak bisa berupa angka negatif ");
				    	
				    }
				    
				    input.nextLine();
					    do{ 
					    	System.out.println();
						    System.out.print("Ulang Kembali, Masukkan Huruf (y/n)");
						    System.out.println();			   
						    choice=input.nextLine();
						    System.out.println("----------------------------------------");
							    if (choice.contentEquals("y"))break;
							    else if(choice.contentEquals("n"))break;
							    else System.out.println("Masukkan input tidak sesuai, tekan 'y' atau 'n'\n\n" );
					    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
					    
					    if (choice.contentEquals("y"))continue;
					    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}


